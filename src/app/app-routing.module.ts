import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { TodosComponent } from './components/todos/todos.component';
import { AddComponent } from './components/add/add.component';
import { SettingsComponent } from './components/settings/settings.component';


const routes: Routes = [
  // Index
  {
    path: 'todos',
    component: TodosComponent
  },
  // Add Todo
  {
    path: 'addTodo',
    component: AddComponent
  },
  // Settings
  {
    path: 'settings',
    component: SettingsComponent
  },
  // / > todos
  {
    path: '',
    redirectTo: 'todos',
    pathMatch: 'full'
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
